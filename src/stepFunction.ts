const applyRules = (currentState, livingNeighbors) => {
	/**
	 * Under-population:
	 * Any live cell with fewer than two live neighbors dies.
	*/
	if (currentState && livingNeighbors < 2) {
		return 0
	}


	/**
	 * Lives:
	 * Any live cell with two or three live neighbors lives on a generation
	*/
	if (currentState && (livingNeighbors === 2 || livingNeighbors === 3)) {
		return 1
	}


	/**
	 * Over-population
	 * Any live cell with more than three live neighbors dies.
	 */
	if (currentState && livingNeighbors > 3) {
		return 0
	}


	/**
	 * Reproduction:
	 * Any dead cell with exactly three living neighbors becomes alive.
	 */
	if (!currentState && livingNeighbors === 3) {
		return 1
	}

	/**
	 * dead cells stay dead
	 */
	if (!currentState && livingNeighbors !== 3) {
		return 0
	}

	throw new Error(`Invalid state reached:\ncurrentState: ${currentState}\nlivingNeighbors: ${livingNeighbors}`)
}


// computes a neighbors array from the current state.
// for the ith element, its neighbors are:
// i +/- N +/- 1 and i +/- 1
const computeNeighbors = (states: number[], rowLength: number) => {
	return states.map( (_, i, arr) => {
		// slice and pick the array to get neighbors.
		const neighbors = [
			(i > 0 ? arr.at(i - 1) : []), //left
			(i < arr.length ? arr.at(i + 1) : []), //right
			// slices are an additional +1 because the end is inclusive.
			arr.slice(i - rowLength - 1, i - rowLength + 2 ), //previous row.
			arr.slice(i + rowLength - 1, i + rowLength + 2 ), //next row
		].flat()

		const livingNeighbors = neighbors.filter(Boolean)
		const neighborsArray = livingNeighbors.length
		return neighborsArray
	})
}

// creates the next state from the neighbors array.
const applyCellRules = (states: number[], neighbors: number[]) => {
	return neighbors.map( (livingNeighbors, i) => {
		const cellState = states.at(i)
        document.getElementById(`${i}`)?.setAttribute('data-state', `${cellState}`)
		return applyRules(cellState, livingNeighbors)
	})
}

// completes one generation of cells.
const step = (initialState, N): number[] => {
	// generate a separate array of nearest living neighbors for each cell.
	const neighborsArray = computeNeighbors(initialState, N)
	// apply the rules to each cell.
	const nextState = applyCellRules(initialState, neighborsArray)
    return nextState
}

export default step
