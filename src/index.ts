#!/bin/node

// given ==+==/=+++=/==+==/=====/
// where N=5
// 1 2 3 4 5
// = = + = =
// = + + + =
// = = + = =
// = = = = =

import step from './stepFunction'


const initialize = (initialState: number[], rowLength: number, cellWidth: number) => {
    // initialize a grid of N x N cells.
    const gridRoot = document.getElementById('grid')
    gridRoot?.style.setProperty('--row-length', `${rowLength}`)
    gridRoot?.style.setProperty('--cell-width', `${cellWidth}px`)

    const cellElements = Array.from({length: rowLength * rowLength}, (_, i) => {
        const cellElem = document.createElement('div')
        cellElem.classList.add('cell')
        const state = initialState[i]
        cellElem.setAttribute('data-state', `${state}`)
        cellElem.id = `${i}`
        return cellElem
    })

    gridRoot!.append(...cellElements)

    STATE = initialState
}


var PLAY: boolean = false
var FRAME_INTERVAL: number = 150
const play = () => {
    if (!PLAY) {
        return
    }
    STATE = step(STATE, N)
    setTimeout(play, FRAME_INTERVAL)
}


const locations = [
    "809",
    "811",
    "812",
    "858",
    "865",
    "907",
    "908",
    "912",
    "915",
    "954",
    "955",
    "957",
    "963",
    "964",
    "1004",
    "1005",
    "1007",
    "1013",
    "1014",
    "1057",
    "1058",
    "1062",
    "1065",
    "1108",
    "1115",
    "1159",
    "1161",
    "1162"
]

var STATE: number[] = []
const N = 50
const initialState = Array.from({length: N * N}, () => 0)
const cellWidth = 15

initialize(initialState, N, cellWidth)

document.getElementById('grid')!.addEventListener('click', (e) => {
    if (e.target instanceof HTMLDivElement) {
        const id = e.target.id
        const state = e.target.dataset.state
        const nextState = state === '0' ? '1' : '0'
        e.target.setAttribute('data-state', nextState)
        STATE[id] = Number(nextState)
    }
})

document.getElementById('start')!.onclick = () => {
    PLAY = true
    play()
}

document.getElementById('stop')!.onclick = () => {
    PLAY = false
}
